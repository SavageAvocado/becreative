package net.savagedev.rcf.listeners;

import net.savagedev.rcf.RCF;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakE implements Listener {
    private RCF plugin;

    public BlockBreakE(RCF plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockBreakE(BlockBreakEvent e) {
        Block block = e.getBlock();
        if (block.getType() == Material.CHEST) {
            Chest chest = (Chest)block.getState();
            if (this.plugin.getCooldowns().containsKey(chest)) {
                this.plugin.removeCooldown(chest);
            }

        }
    }
}
