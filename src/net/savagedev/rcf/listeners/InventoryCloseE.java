package net.savagedev.rcf.listeners;

import net.savagedev.rcf.RCF;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class InventoryCloseE implements Listener {
    private RCF plugin;

    public InventoryCloseE(RCF plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryCloseE(InventoryCloseEvent e) {
        InventoryHolder inventoryHolder = e.getInventory().getHolder();
        Integer inventoryContents = 0;
        if (inventoryHolder instanceof Chest) {
            ItemStack[] var4 = inventoryHolder.getInventory().getContents();
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                ItemStack stack = var4[var6];
                if (stack != null) {
                    inventoryContents = inventoryContents + 1;
                }
            }

            if (inventoryContents <= this.plugin.getMinRefillAmount() && !this.plugin.getCooldowns().containsKey(e.getInventory().getHolder())) {
                this.plugin.addCooldown((Chest)e.getInventory().getHolder(), System.currentTimeMillis());
            }

        }
    }
}
