package net.savagedev.rcf.listeners;

import net.savagedev.rcf.RCF;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class InteractE implements Listener {
    private Integer maxRefillAmount;
    private Integer cooldownTime;
    private Boolean clearChests;
    private RCF plugin;

    public InteractE(RCF plugin) {
        this.plugin = plugin;
        this.cooldownTime = this.plugin.getConfig().getInt("refill-time");
        this.clearChests = this.plugin.getConfig().getBoolean("clear-on-refill");
        this.maxRefillAmount = this.plugin.getConfig().getInt("max-refill-amount");
    }

    @EventHandler
    public void onInteractE(PlayerInteractEvent e) {
        Block block = e.getClickedBlock();
        Action action = e.getAction();
        Integer inventoryContents = 0;
        if (action == Action.RIGHT_CLICK_BLOCK) {
            if (block.getType() == Material.CHEST) {
                Chest chest = (Chest)block.getState();
                ItemStack[] var6 = chest.getInventory().getContents();
                int var7 = var6.length;

                for(int var8 = 0; var8 < var7; ++var8) {
                    ItemStack item = var6[var8];
                    if (item != null) {
                        inventoryContents = inventoryContents + 1;
                    }
                }

                if (inventoryContents > this.plugin.getMinRefillAmount()) {
                    if (!this.plugin.getCooldowns().containsKey(chest)) {
                        return;
                    }

                    this.plugin.removeCooldown(chest);
                }

                if (this.plugin.getCooldowns().containsKey(chest)) {
                    Long secondsLeft = (Long)this.plugin.getCooldowns().get(chest) / 1000L + (long)this.cooldownTime - System.currentTimeMillis() / 1000L;
                    if (secondsLeft <= 0L) {
                        this.plugin.removeCooldown(chest);
                        if (this.clearChests) {
                            chest.getInventory().clear();
                            inventoryContents = 0;
                        }

                        while(inventoryContents < this.maxRefillAmount) {
                            Integer random = (new Random()).nextInt(this.plugin.getChances().size());
                            ItemStack item = (ItemStack)this.plugin.getItemFromChances().get(random);
                            Integer slot = (new Random()).nextInt(chest.getInventory().getSize());
                            if ((new Random()).nextInt(99) + 1 <= this.plugin.getChance(item) && chest.getInventory().getItem(slot) == null) {
                                chest.getBlockInventory().setItem(slot, item);
                                inventoryContents = inventoryContents + 1;
                            }
                        }

                    }
                }
            }
        }
    }
}
