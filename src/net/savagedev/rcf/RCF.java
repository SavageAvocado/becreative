package net.savagedev.rcf;

import net.savagedev.rcf.listeners.BlockBreakE;
import net.savagedev.rcf.listeners.InteractE;
import net.savagedev.rcf.listeners.InventoryCloseE;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class RCF extends JavaPlugin {
    private HashMap<ItemStack, Integer> chances;
    private HashMap<Chest, Long> cooldown;
    private Integer minRefillAmount;

    public RCF() {
    }

    public void onEnable() {
        this.loadUtils();
        this.loadConfig();
        this.loadCommands();
        this.loadListeners();
        this.loadItemChances();
    }

    private void loadUtils() {
        this.minRefillAmount = this.getConfig().getInt("min-refill-amount");
        this.cooldown = new HashMap();
        this.chances = new HashMap();
    }

    private void loadConfig() {
        this.saveDefaultConfig();
    }

    private void loadCommands() {
    }

    private void loadListeners() {
        PluginManager pluginManager = this.getServer().getPluginManager();
        pluginManager.registerEvents(new InteractE(this), this);
        pluginManager.registerEvents(new BlockBreakE(this), this);
        pluginManager.registerEvents(new InventoryCloseE(this), this);
    }

    private void loadItemChances() {
        Iterator var1 = this.getConfig().getConfigurationSection("items").getKeys(false).iterator();

        while (var1.hasNext()) {
            String path = (String) var1.next();
            this.chances.put(new ItemStack(Material.valueOf(this.getConfig().getString("items." + path + ".item").toUpperCase()), this.getConfig().getInt("items." + path + ".amount")), this.getConfig().getInt("items." + path + ".chance"));
        }

    }

    public Integer getMinRefillAmount() {
        return this.minRefillAmount;
    }

    public HashMap<Chest, Long> getCooldowns() {
        return this.cooldown;
    }

    public void removeCooldown(Chest chest) {
        this.cooldown.remove(chest);
    }

    public void addCooldown(Chest chest, Long time) {
        this.cooldown.put(chest, time);
    }

    public HashMap<ItemStack, Integer> getChances() {
        return this.chances;
    }

    public ArrayList<ItemStack> getItemFromChances() {
        return new ArrayList(this.chances.keySet());
    }

    public Integer getChance(ItemStack itemStack) {
        return (Integer) this.chances.get(itemStack);
    }
}
